package com.example.licenta_fr.test;

import java.io.File;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.support.annotation.BoolRes;

import com.sromku.simple.storage.SimpleStorage;
import com.sromku.simple.storage.Storage;

import model.Person;
import junit.framework.TestCase;

public class PersonTest extends TestCase {
	//Constants for checks
	public static List<Person> testPersonListGood = new ArrayList<Person>();
	public static List<Person> testPersonListBad = new ArrayList<Person>();
	public static boolean setUpBeforeClassIsStupid = true;
	
	//Constants for getters and setters
	public static Person personForGetAndSet = null;
	
	public static long ID = 123456789;
	public static int FACE_ID = 1;
	public static String CNP = "1921013430027";
	public static String NAME = "Tanase";
	public static String FAMILY_NAME = "Cucliciu";
	public static String MARITAL_STATE = "Celibate";
	public static String OCCUPATION = "Student";
	public static String PHOTO_PATH = "TOBETESTED/";
	public static boolean SEX = false;
	public static Date DATE_OF_BIRTH = new Date();
	
	
	public static void initializePersonList(List<Person> testPersonList, String fileName){
		try{
			Storage storage = SimpleStorage.getExternalStorage();
			String file = storage.readTextFile("Test", fileName);
			String[] lines = file.split("\n");
		    for(int i =0; i < lines.length; i++){
			    String [] columns = lines[i].split("\\s+");	
			    DateFormat formatter = new SimpleDateFormat("d-MMM-yyyy,HH:mm:ss aaa");
			    Date date = formatter.parse(columns[8] + " " + columns[9]);
			    
			    Person person = new Person();
			    person.setId(Integer.parseInt(columns[0]));
			    person.setFaceId(Integer.parseInt(columns[1]));
			    person.setCNP(columns[2]);
			    person.setName(columns[3]);
			    person.setFamilyName(columns[4]);
			    person.setMaritalState(columns[5]);
			    person.setOccupation(columns[6]);
			    person.setPhotoFilePath(columns[7]);
			    person.setDateOfBirth(date);
			    person.setSex(Boolean.valueOf(columns[10]));
			    testPersonList.add(person);
		    }
	    }catch(Exception e){
	    	System.out.println("Err: " + e.getLocalizedMessage());
	    }
	}
	
	protected static void setUpBeforeClass() throws Exception {
		if(setUpBeforeClassIsStupid){
			initializePersonList(testPersonListBad, "data_bad.txt");
			initializePersonList(testPersonListGood, "data_good.txt");
			setUpBeforeClassIsStupid = false;
		}
	}

	protected static void tearDownAfterClass() throws Exception {
		if(setUpBeforeClassIsStupid){
			/*testPersonListGood = null;
			testPersonListBad = null;*/
		}
	}

	protected void setUp() throws Exception {
		if(setUpBeforeClassIsStupid){
			personForGetAndSet = new Person();
			personForGetAndSet.setId(ID);
			personForGetAndSet.setFaceId(FACE_ID);
			personForGetAndSet.setCNP(CNP);
			personForGetAndSet.setName(NAME);
			personForGetAndSet.setFamilyName(FAMILY_NAME);
			personForGetAndSet.setMaritalState(MARITAL_STATE);
			personForGetAndSet.setOccupation(OCCUPATION);
			personForGetAndSet.setPhotoFilePath(PHOTO_PATH);
			personForGetAndSet.setDateOfBirth(DATE_OF_BIRTH);
			personForGetAndSet.setSex(SEX);
		}
		setUpBeforeClass();
	}

	protected void tearDown() throws Exception {
		tearDownAfterClass();
	}

	public void testPerson() {
		Person person = new Person();
		assertTrue(person.getClass().equals(Person.class));
	}

	public void testPersonLong() {
		boolean testSuccess = true;
		Person person = new Person(ID);
		if(!person.getClass().equals(Person.class) || person.getid() != ID){
			testSuccess = false;
		}
		assertTrue(testSuccess);
	}

	public void testPersonString() {
		boolean testSuccess = true;
		Person person = new Person(ID, FACE_ID, NAME);
		if(!person.getClass().equals(Person.class) 
				|| person.getFaceId() != FACE_ID 
				|| person.getid() != ID 
				|| !person.getName().equalsIgnoreCase(NAME)){
			testSuccess = false;
		}
		assertTrue(testSuccess);
	}

	public void testGetName() {
		assertEquals(personForGetAndSet.getName(),NAME);
	}

	public void testSetName() {
		String name = "Mihai";
		personForGetAndSet.setName(name);
		assertEquals(personForGetAndSet.getName(),name);
	}

	public void testGetFamilyName() {
		assertEquals(personForGetAndSet.getFamilyName(),FAMILY_NAME);
	}

	public void testSetFamilyName() {
		String familyName = "Shinoda";
		personForGetAndSet.setFamilyName(familyName);
		assertEquals(personForGetAndSet.getFamilyName(),familyName);
	}

	public void testIsSex() {
		assertEquals(personForGetAndSet.isSex(),SEX);
	}

	public void testSetSex() {
		boolean sex = !SEX;
		personForGetAndSet.setSex(sex);
		assertEquals(personForGetAndSet.isSex(),sex);
	}

	public void testGetDateOfBirth() {
		assertEquals(personForGetAndSet.getDateOfBirth(),DATE_OF_BIRTH);
	}

	public void testSetDateOfBirth() {
		Date date = new Date();
		personForGetAndSet.setDateOfBirth(date);
		assertEquals(personForGetAndSet.getDateOfBirth(),date);
	}

	public void testGetOccupation() {
		assertEquals(personForGetAndSet.getOccupation(),OCCUPATION);
	}

	public void testSetOccupation() {
		String occupation = "Lawyer";
		personForGetAndSet.setOccupation(occupation);
		assertEquals(personForGetAndSet.getOccupation(),occupation);
	}

	public void testGetMaritalState() {
		assertEquals(personForGetAndSet.getMaritalState(),MARITAL_STATE);
	}

	public void testSetMaritalState() {
		String marital = "Married";
		personForGetAndSet.setMaritalState(marital);
		assertEquals(personForGetAndSet.getMaritalState(),marital);
	}

	public void testGetPhotoFilePath() {
		assertEquals(personForGetAndSet.getPhotoFilePath(),PHOTO_PATH);
	}

	public void testSetPhotoFilePath() {
		String photo = "Testing/ceva";
		personForGetAndSet.setPhotoFilePath(photo);
		assertEquals(personForGetAndSet.getPhotoFilePath(),photo);
	}

	public void testGetFaceId() {
		assertEquals(personForGetAndSet.getFaceId(),FACE_ID);
	}

	public void testSetFaceId() {
		int faceID = 3;
		personForGetAndSet.setFaceId(faceID);
		assertEquals(personForGetAndSet.getFaceId(),faceID);
	}

	public void testGetid() {
		assertEquals(personForGetAndSet.getid(),ID);
	}

	public void testSetId() {
		int id = 2;
		personForGetAndSet.setId(id);
		assertEquals(personForGetAndSet.getid(),id);
	}

	public void testGetFullName() {
		assertEquals(personForGetAndSet.getFullName(),NAME + " " + FAMILY_NAME);
	}

	public void testGetCNP() {
		assertEquals(personForGetAndSet.getCNP(),CNP);
	}

	public void testSetCNP() {
		String cnp = "1720906237348";
		personForGetAndSet.setCNP(cnp);
		assertEquals(personForGetAndSet.getCNP(),cnp);
	}

	public void testFindByFaceId() {
		assertEquals(Person.findByFaceId(testPersonListGood, 1).getid(),1);
	}

	public void testCheckID() {
		boolean testSuccess = true;
		for(Person person : testPersonListBad){
			if(person.checkID(person.getid())) {
				testSuccess = false;
				break;
			}
		}
		
		for(Person person : testPersonListGood){
			if(!person.checkID(person.getid())) {
				testSuccess = false;
				break;
			}
		}
		
		assertTrue(testSuccess);
	}

	public void testCheckName() {
		
		boolean testSuccess = true;
		for(Person person : testPersonListBad){
			if(person.checkName(person.getName())) {
				testSuccess = false;
				break;
			}
		}
		
		for(Person person : testPersonListGood){
			if(!person.checkName(person.getName())) {
				testSuccess = false;
				break;
			}
		}
		
		assertTrue(testSuccess);
	}

	public void testCheckPhotoFilePath() {
		boolean testSuccess = true;
		for(Person person : testPersonListBad){
			if(person.checkName(person.getName())) {
				testSuccess = false;
				break;
			}
		}
		
		for(Person person : testPersonListGood){
			if(!person.checkName(person.getName())) {
				testSuccess = false;
				break;
			}
		}
		
		assertTrue(testSuccess);
	}

	public void testCheckSex() {
		boolean testSuccess = true;
		for(Person person : testPersonListBad){
			if(person.checkSex(person.isSex())) {
				testSuccess = false;
				break;
			}
		}
		
		for(Person person : testPersonListGood){
			if(!person.checkSex(person.isSex())) {
				testSuccess = false;
				break;
			}
		}
		
		assertTrue(testSuccess);
	}

	public void testCheckCNP() {
		boolean testSuccess = true;
		for(Person person : testPersonListBad){
			if(person.checkCNP(person.getCNP())) {
				testSuccess = false;
				break;
			}
		}
		
		for(Person person : testPersonListGood){
			if(!person.checkCNP(person.getCNP())) {
				testSuccess = false;
				break;
			}
		}
		
		assertTrue(testSuccess);
	}

	public void testCheckStringNull() {
		boolean testSuccess = true;
		for(Person person : testPersonListBad){
			person.setMaritalState("");
			person.setOccupation("");
			if(!person.checkStringNull((person.getOccupation()))) {
				testSuccess = false;
				break;
			}
			if(!person.checkStringNull((person.getMaritalState()))) {
				testSuccess = false;
				break;
			}
		}
		
		for(Person person : testPersonListGood){
			if(person.checkStringNull((person.getOccupation()))) {
				testSuccess = false;
				break;
			}
			if(person.checkStringNull((person.getMaritalState()))) {
				testSuccess = false;
				break;
			}
		}
		
		assertTrue(testSuccess);
		
		setUpBeforeClassIsStupid = true;
	}

}
