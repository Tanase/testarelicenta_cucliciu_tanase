package com.example.licenta_fr.test;

import java.util.ArrayList;
import java.util.List;

import org.mockito.Mockito;
import model.IOnTouchListenerStrategy;
import model.Person;
import helper.Data;
import android.test.InstrumentationTestCase;
import android.util.Log;

import static org.mockito.Mockito.*;
import com.example.licenta_fr.MainActivity;
import com.qualcomm.snapdragon.sdk.face.FacialProcessing;

import junit.framework.TestCase;

public class OtherTests extends InstrumentationTestCase {

	
	
	protected static void setUpBeforeClass() throws Exception {
		
	}

	protected static void tearDownAfterClass() throws Exception {
	}

	protected void setUp() throws Exception {
		super.setUp();
		System.setProperty("dexmaker.dexcache", getInstrumentation()
				.getTargetContext().getCacheDir().getPath());
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testGetDBInfo() {
		try{
			/*MainActivity.FACIAL_PROCESSING = Mockito.mock(FacialProcessing.class);
			Data._singleton = Mockito.mock(Data.class);
			when(MainActivity.FACIAL_PROCESSING.getAlbumPersonCount()).thenReturn(0);
			when(Data._singleton.getPersonCountSQLDB()).thenReturn(0);
			
			String result = MainActivity.getDBInfo();
			String expected = "There are 0 persons \n in the Database";
			
			assertEquals(result, expected);*/
		}catch(Exception e){
			Log.e("ERROR", e.getLocalizedMessage());
		}
	}
	
	public void testPad() {
		try{
			boolean testSuccess = true;
			String result = Data.pad(10);
			String expected = "10";
			if(!result.equalsIgnoreCase(expected)) testSuccess = false;
			
			result = Data.pad(11);
			expected = "11";
			if(!result.equalsIgnoreCase(expected)) testSuccess = false;
			
			result = Data.pad(1);
			expected = "01";
			if(!result.equalsIgnoreCase(expected)) testSuccess = false;
			
			assertTrue(testSuccess);
		}catch(Exception e){
			Log.e("ERROR", e.getLocalizedMessage());
		}
	}
	
	public void testgetPersonCountSQLDB() {
		try{
			/*Data._singleton = null;
			int nr = Data.getInstance().getPersonCountSQLDB();
			assertEquals(39, nr);*/
		}catch(Exception e){
			Log.e("ERROR", e.getLocalizedMessage());
		}
	}
	
	public void testgetLastPersonID() {
		try{
			long nr = Data.getInstance().getLastPersonID();
			assertEquals(39, nr);
		}catch(Exception e){
			Log.e("ERROR", e.getLocalizedMessage());
		}
	}

}
